import CarsApi from '../../tools/CarsApi';

const carsLoadAction = () => dispatch =>{
	let Api = new CarsApi();
	Api.get('/car').then(function(response){
		console.log('Api get /var', response);
		
		if(response.status == 200){
			dispatch({
				type:'TYPE_CARS_LOADED',
				data:response.data.data
			});
		}
	});
	
};

export default carsLoadAction;
