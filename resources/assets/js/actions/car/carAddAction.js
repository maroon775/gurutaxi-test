import CarsApi from '../../tools/CarsApi';

const carAddAction = (data) => dispatch => {
	let Api = new CarsApi();
	
	Api.api.post('/car/',data).then(function(response){
		console.log('Api POST /car/', response);
		
		if(response.data.status == 201)
		{
			dispatch({
				type:'TYPE_CARS_ADDED',
				data:response.data.data
			});
		}
	});
	
};

export default carAddAction;
