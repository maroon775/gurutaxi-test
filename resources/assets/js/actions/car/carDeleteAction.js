import CarsApi from '../../tools/CarsApi';
import carsLoadAction from '../cars/carsLoadAction';

const carDeleteAction = (id) => dispatch => {
	let Api = new CarsApi();
	
	Api.api.delete('/car/' + id).then(function(response){
		console.log('Api DELETE /car/' + id, response);
		
		if(response.data.status == 200)
		{
			dispatch(carsLoadAction())
		}
	});
	
};

export default carDeleteAction;
