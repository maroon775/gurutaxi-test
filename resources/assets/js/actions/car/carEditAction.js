import CarsApi from '../../tools/CarsApi';

const carEditAction = (id,data) => dispatch => {
	let Api = new CarsApi();
	
	Api.api.put('/car/' + id,data).then(function(response){
		console.log('Api PUT /car/' + id, response);
		
		if(response.data.status == 200)
		{
			dispatch({
				type:'TYPE_CAR_EDITED',
				data:response.data.data
			});
		}
	});
	
};

export default carEditAction;
