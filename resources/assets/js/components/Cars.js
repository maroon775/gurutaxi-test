import React, {Component} from 'react';
import {connect} from "react-redux";
import carsLoadAction from "../actions/cars/carsLoadAction";
import Car from './Car';

class Cars extends Component {
	constructor(props)
	{
		super(props);
		
		if(this.props.cars.length <= 0)
		{
			this.props.loadCars();
		}
	}
	
	render()
	{
		let cars = null;
		if(this.props.cars.length > 0)
		{
			cars = this.props.cars.map((car,i)=>(<Car key={car.id} carId={car.id} title={car.title} />));
		}
		
		return (<div className="autopark__list">{cars}</div>);
	}
}

export default connect(
	state => ({
		cars:state.carsReducer
	}),
	dispatch => ({
		loadCars:() =>
			dispatch(carsLoadAction()),
	})
)(Cars);
