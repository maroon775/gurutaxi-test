import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import carAddAction from "../actions/car/carAddAction";

class AddCarForm extends Component {
	constructor(props)
	{
		super(props);
		this.obNodeInput = null;
		this.obNodeButton = null;
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onInputWrite = this.onInputWrite.bind(this);
	}
	
	render()
	{
		return (<form className="autopark-addCar" onSubmit={this.onFormSubmit}>
			<input onKeyUp={this.onInputWrite} className="autopark-addCar__input" ref={input => this.obNodeInput = input} type="text"/>
			<button className="autopark-addCar__button" ref={button => this.obNodeButton = button} type="submit">Add</button>
		</form>)
	}
	
	onInputWrite()
	{
		if(this.obNodeInput.value.length >= 3)
		{
			this.obNodeButton.classList.add('autopark-addCar__button_activate');
		}
		else
		{
			this.obNodeButton.classList.remove('autopark-addCar__button_activate');
		}
	}
	
	onFormSubmit(e)
	{
		e.preventDefault();
		if(this.obNodeInput.value.length <= 0)
		{
			return false;
		}
		
		this.props.addCar({title:this.obNodeInput.value});
		
		this.obNodeButton.classList.remove('autopark-addCar__button_activate');
		this.obNodeInput.value = '';
		return false;
	}
}

export default connect(
	state => ({}),
	dispatch => ({
		addCar:(data) => {
			dispatch(carAddAction(data))
		}
	})
)(AddCarForm);
