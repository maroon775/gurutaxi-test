import React, {Component} from 'react';
import ReactDom from 'react-dom';
import store from 'store';
import connect from "react-redux/es/connect/connect";
import carDeleteAction from "../actions/car/carDeleteAction";
import carEditAction from "../actions/car/carEditAction";

class Car extends Component {
	constructor(props)
	{
		super(props);
		
		this.title = this.props.title;
		this.obNodeInput = null;
		this.state = {
			edit:false
		};
		
		this.onClickEditToggle = this.onClickEditToggle.bind(this);
		this.onClickSave = this.onClickSave.bind(this);
		this.onClickDelete = this.onClickDelete.bind(this);
	}
	
	render()
	{
		console.log(this.props);
		let carElement;
		if(this.state.edit === false)
		{
			carElement = <div className="autopark-item">
				<div className="autopark-item__title">{this.title}</div>
				<div className="autopark-item__buttons">
					<button className="autopark-item__button autopark-item__button_edit" onClick={this.onClickEditToggle}>Edit</button>
					<button className="autopark-item__button autopark-item__button_delete" onClick={this.onClickDelete}>Delete</button>
				</div>
			</div>;
		}
		else
		{
			carElement = <div className="autopark-item">
				<div className="autopark-item__title"><input autofocus="true" className="autopark-item__input" type="text" ref={(obNode) => {
				this.obNodeInput = obNode;
				}} defaultValue={this.title}/></div>
				<div className="autopark-item__buttons">
					<button className="autopark-item__button autopark-item__button_save" onClick={this.onClickSave}>Save</button>
					<button className="autopark-item__button autopark-item__button_cancel" onClick={this.onClickEditToggle}>Cancel</button>
				</div>
			</div>;
		}
		return (carElement)
	}
	
	onClickEditToggle(e)
	{
		this.setState({edit:!this.state.edit});
		return false;
	}
	
	onClickSave(e)
	{
		this.title = this.obNodeInput.value;
		this.props.carEdit(this.props.carId,{title:this.title});
		
		this.setState({edit:false});
		return false;
	}
	
	onClickDelete(e)
	{
		this.props.carDelete(this.props.carId);
	}
}

export default connect(
	state => ({
	}),
	dispatch => ({
		carDelete:(id) =>
			dispatch(carDeleteAction(id)),
		carEdit  :(id,data) =>
			dispatch(carEditAction(id,data)),
	})
)(Car);

