import store from 'store';

export default function carsReducer(state = [], action){
// 	console.log('TYPE_CARS_ADDED', action);
	switch(action.type)
	{
		case 'TYPE_CARS_LOADED':
			return action.data;
		case 'TYPE_CARS_ADDED':
			return [...state,action.data];
		default:
			return state;
	}
	
}
