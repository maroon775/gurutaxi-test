import axios from 'axios';

const API_PATH = 'http://127.0.0.1:8000/api/';

class CarsApi {
	constructor(params)
	{
		this.api = axios.create({baseURL:API_PATH});
	}
	
	get(path, params)
	{
		return this.api.get(path, params);
	}
}

export default CarsApi;
