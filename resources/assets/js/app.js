import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {composeWithDevTools} from 'redux-devtools-extension';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import reducer from './reducers/index';

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
import Cars from './components/Cars';
import AddCarForm from './components/AddCarForm';


ReactDOM.render((<Provider store={store}>
	<div className="autopark">
		<Cars/>
		<AddCarForm/>
	</div>
</Provider>), document.getElementById('carsApp'));



