<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use Illuminate\Http\Response;
use App\Http\Requests\CarRequest;

/**
 * @access  public
 * @author  Amal
 * @version 1.0.0
 */
class CarController extends Controller {
	protected $request;
	protected $car;

	/**
	 *
	 * @param Request $request
	 * @param Car     $car
	 */
	public function __construct( Request $request, Car $car ) {
		$this->request = $request;
		$this->car     = $car;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$cars = $this->car->all();

		return response()->json( [
			'data'   => $cars,
			'status' => Response::HTTP_OK
		] );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( CarRequest $request ) {
		$data             = $this->request->all();
		$this->car->title = $data['title'];
		$this->car->save();

		return response()->json( [ 'status' => Response::HTTP_CREATED, 'data' => $this->car ] );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$data = $request->all();

		$car        = $this->car->find( $id );
		$car->title = $data['title'];
		$car->save();

		return response()->json( [ 'status' => Response::HTTP_OK, 'data' => $car ] );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		$car = $this->car->find( $id );

		return response()->json( [ 'status' => Response::HTTP_OK, 'data' => $car ] );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		$car = $this->car->find( $id );

		if ( $car ) {
			$car->delete();

			return response()->json( [ 'status' => Response::HTTP_OK ] );
		} else {
			return response()->json( [ 'status' => Response::HTTP_NOT_FOUND ] );
		}
	}

}
