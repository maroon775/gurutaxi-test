<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * CarRequest
 *
 * @version 1.0.0
 * @since 1.0.0
 * @author Amal
 *
 */
class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    return [
		    'id'    => 'int|max:11|unique:id',
		    'title' => 'required|max:255',
	    ];
    }
}
