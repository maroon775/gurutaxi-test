<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Car
 *
 * @version 1.0.0
 * @since 1.0.0
 * @author Amal
 *
 */
class Car extends Model {

	public $timestamps = [];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * Set the user's name.
	 *
	 * @param  string $value
	 *
	 * @return void
	 */
	public function setTitleAttribute( $value ) {
		return $this->attributes['title'] = $value;
	}

}
